
protected function discriminateTrial():void {
	var trial:Array = Trials[CurrentTrial]
//	continueButton.enabled = false;
	discriminateVideo1.emphasized = false;
	discriminateVideo2.emphasized = false;
	CurrentNetStream = Videos[trial[1]];
	CurrentNetStream2 = Videos[trial[2]];
	QueuedSounds = [];
	for each(var word:String in trial[3].split(" ")) {
		QueuedSounds.push(Sounds[word]);
	}
	// check whether everything is loaded
	if((CurrentNetStream.bytesLoaded >= CurrentNetStream.bytesTotal-100) &&
	   (CurrentNetStream2.bytesLoaded >= CurrentNetStream2.bytesTotal-100)) {
		discriminateTrialGo();
	} else {
		var prog:ProgressPopup =  new ProgressPopup();
		PopUpManager.addPopUp(prog, this, true);
		PopUpManager.centerPopUp(prog);
		// use a timer or something, or connect directly to netstream?
		PopupInterval = setInterval(checkPreload, 100, prog);
	}
}

protected function discriminateTrialGo():void {
	var trial:Array = Trials[CurrentTrial];
	var sent:String = trial[3];
	sent = sent.replace(/_/g, ' ');
	discriminateSentence.text = sent;
	CurrentNetStream.seek(0);
	CurrentNetStream2.seek(0);
	CurrentNetStream.resume();
	CurrentNetStream2.resume();
	discriminateVideo1.setStyle("fillAlphas", [0, 0]);
	discriminateVideo2.setStyle("fillAlphas", [0, 0]);
	discriminateVideo1.emphasized = false;
	discriminateVideo2.emphasized = false;
	CurrentlyPlaying = -1;
	playSounds(0);
}

