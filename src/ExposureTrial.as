
protected function exposureTrial():void {
	var trial:Array = Trials[CurrentTrial]
	CurrentNetStream = Videos[trial[1]];
	CurrentNetStream2 = null;
	QueuedSounds = [];
	for each(var word:String in trial[2].split(" ")) {
		QueuedSounds.push(Sounds[word]);
	}
	// check whether everything is loaded
	if(CurrentNetStream.bytesLoaded >= CurrentNetStream.bytesTotal-100) {
		exposureTrialGo();
	} else {
		var prog:ProgressPopup =  new ProgressPopup();
		PopUpManager.addPopUp(prog, this, true);
		PopUpManager.centerPopUp(prog);
		// use a timer or something, or connect directly to netstream?
		PopupInterval = setInterval(checkPreload, 100, prog);
	}
}

protected function exposureTrialGo():void {
	var trial:Array = Trials[CurrentTrial];
	var sent:String = trial[2];
	sent = sent.replace(/_/g, ' ');
	exposureSentence.text = sent;
	CurrentNetStream.seek(0);
	CurrentNetStream.resume();
	CurrentlyPlaying = -1;
	playSounds(0);
}
