protected function fourPictureTrial():void {
	var trial:Array = Trials[CurrentTrial];
	var dispOrder:Array = [1,2,3,4];
	dispOrder = shuffle(dispOrder);
	CorrectAnswer = dispOrder.indexOf(1);

	pic1.load(Images[trial[dispOrder[0]]].source);
	pic2.load(Images[trial[dispOrder[1]]].source);
	pic3.load(Images[trial[dispOrder[2]]].source);
	pic4.load(Images[trial[dispOrder[3]]].source);
	QueuedSounds = [];
	QueuedSounds.push(Sounds[trial[5]]);
	fourPictureTrialGo();
}

protected function fourPictureTrialGo():void {
	var trial:Array = Trials[CurrentTrial];
	var sent:String = trial[5];

	pic1button.emphasized = false;
	pic2button.emphasized = false;
	pic3button.emphasized = false;
	pic4button.emphasized = false;

	pic1button.setStyle("fillColors", ['0xffffff', '0xcccccc', '0xffffff', '0xeeeeee']);
	pic2button.setStyle("fillColors", ['0xffffff', '0xcccccc', '0xffffff', '0xeeeeee']);
	pic3button.setStyle("fillColors", ['0xffffff', '0xcccccc', '0xffffff', '0xeeeeee']);
	pic4button.setStyle("fillColors", ['0xffffff', '0xcccccc', '0xffffff', '0xeeeeee']);

	pic1button.setStyle("fillAlphas", [0, 0]);
	pic2button.setStyle("fillAlphas", [0, 0]);
	pic3button.setStyle("fillAlphas", [0, 0]);
	pic4button.setStyle("fillAlphas", [0, 0]);

	fourPicSentence.text = sent;
	CurrentlyPlaying = -1;
	playSounds(0);
}
