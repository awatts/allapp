import mx.collections.Sort;
import flash.events.MouseEvent;
import mx.collections.ArrayCollection;
import mx.controls.LinkButton;

private function productionAnswerClick(event:MouseEvent):void {
	// really? this is how you copy an array..?
	var n:ArrayCollection = new ArrayCollection(ProductionAnswer.source.slice());
	n.removeItemAt(event.currentTarget.instanceIndex);
	QueuedSounds = [];
	for each(var s:String in n.source) {
		QueuedSounds.push(Sounds[s]);
	}
	if(n.length == 0) {
//		continueButton.enabled = false;
	}
	ProductionAnswer = n;
}

private function productionVocabClick(event:MouseEvent):void {
	if(currentState == 'Production' || currentState == 'CuedProduction') {
		var n:ArrayCollection = new ArrayCollection(ProductionAnswer.source.slice());
		n.addItemAt(event.currentTarget.label, n.length);
		QueuedSounds = [];
		for each(var s:String in n.source) {
			QueuedSounds.push(Sounds[s]);
		}
		CurrentlyPlaying = QueuedSounds.length-2;
		playSounds(0);
		ProductionAnswer = n;
	} else if(currentState == 'Typing') {
		trace('typing');
		QueuedSounds = [Sounds[event.currentTarget.label]];
		CurrentlyPlaying = -1;
		playSounds(0);
	} else if(currentState == 'ProductionSingle') {
		trace('productionsingle');
		for(var lbi:int=0; lbi<productionVocab.length; lbi++) {
			var lb:LinkButton = productionVocab[lbi] as LinkButton;
			if(lb != event.currentTarget) lb.selected = false;
		}
		event.currentTarget.selected = true;
		ProductionAnswer = new ArrayCollection([event.currentTarget.label]);
		QueuedSounds = [Sounds[event.currentTarget.label]];
		CurrentlyPlaying = -1;
		playSounds(0);
	}
	trace(currentState);
}


private function productionVocabClickProduction(event:MouseEvent):void {
	var n:ArrayCollection = new ArrayCollection(ProductionAnswer.source.slice());
	n.addItemAt(event.currentTarget.label, n.length);
	QueuedSounds = [];
	for each(var s:String in n.source) {
		QueuedSounds.push(Sounds[s]);
	}
	CurrentlyPlaying = QueuedSounds.length-2;
	playSounds(0);
	ProductionAnswer = n;
}


protected function setUpProductionVocab(including:ArrayCollection):void {
	// make a vocab list to choose from
	var vocab:ArrayCollection = new ArrayCollection(including.source.slice());
	var words:Array = getKeys(Sounds);
	// exclude anything that starts with an underscore
	for(var i:int=words.length-1; i>=0; i--) {
		if(words[i].substr(0, 1) == "_") {
			words.splice(i, 1);
		}
	}
	while(vocab.length < Math.min(words.length, 14)) {
		var word:String = vocab[0];
		while(vocab.contains(word)) {
			word = words[Math.floor(Math.random() * words.length)];
		}
		vocab.addItem(word);
	}
	ProductionVocabChoices = vocab;
	ProductionVocabChoices.sort = new Sort();
	ProductionVocabChoices.refresh();
}

protected function productionTrial():void {
	var trial:Array = Trials[CurrentTrial];
	var prod_vocab:ArrayCollection = new ArrayCollection(trial[2].split(" "));
	CurrentNetStream = Videos[trial[1]];
	CurrentNetStream2 = null;
	setUpProductionVocab(prod_vocab);
	ProductionAnswer = new ArrayCollection();
	QueuedSounds = [];
	for(var lbi:int=0; lbi<productionVocab.length; lbi++) {
		var lb:LinkButton = productionVocab[lbi] as LinkButton;
		// in an multi word production trial, we don't want toggle buttons
		lb.toggle = false;
		//if this is a cued production trial put a red border on last vocab word
		if(currentState == 'CuedProduction') {
			if(lb.label == prod_vocab[prod_vocab.length-1]) {
				lb.setStyle('color', 'red');
			}
		}
	}

	// check whether everything is loaded
	if(CurrentNetStream.bytesLoaded >= CurrentNetStream.bytesTotal-100) {
		productionTrialGo();
	} else {
		var prog:ProgressPopup =  new ProgressPopup();
		PopUpManager.addPopUp(prog, this, true);
		PopUpManager.centerPopUp(prog);
		// use a timer or something, or connect directly to netstream?
		PopupInterval = setInterval(checkPreload, 100, prog);
	}
//	continueButton.enabled = canContinue();
	replayButton.enabled = replayButton.visible // since there's no sound playing to wait for
}
protected function productionTrialGo():void {
	var trial:Array = Trials[CurrentTrial];
	CurrentNetStream.seek(0);
	CurrentNetStream.resume();
	CurrentlyPlaying = -1;
	playSounds(0);
}
