// misc small helper functions that are used throughout go in this file

public static function shuffle(arr:Array):Array {
	var arr2:Array = new Array();
	while (arr.length > 0) {
		arr2.push(arr.splice(Math.floor(Math.random() * arr.length), 1)[0]);
	}
	return arr2;
}

public static function shuffleAC(arr:ArrayCollection):ArrayCollection {
	var arr2:ArrayCollection = new ArrayCollection();
	while (arr.length > 0) {
		arr2.addItem(arr.removeItemAt(Math.floor(Math.random() * arr.length)));
	}
	return arr2;
}

public static function getKeys(d:Dictionary):Array {
	var a:Array = new Array();
	for (var key:Object in d) {
		a.push(key);
	}
	return a;
}

public static function getValues(d:Dictionary):Array {
	var a:Array = new Array();
	for (var key:Object in d) {
		a.push(d[key]);
	}
	return a;
}
