#!/usr/bin/env bash

export FLEX_LIB='/usr/local/flex_sdk-4.14.1'
export DS_LIB='/usr/local/polygonal-ds-latest/lib/'
${FLEX_LIB}/bin/mxmlc -creator "Hal Tily and Andrew Watts" -title "allapp" -localized-description "Artificial Language Learning Applet" en-US -library-path+=${DS_LIB} -compiler.debug -compiler.include-libraries=${DS_LIB}/ds.swc allapp.mxml -output=allapp_flex_4.14.swf
