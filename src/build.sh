#!/usr/bin/env bash
export JAVA_HOME="$(/usr/libexec/java_home -v 1.6)" # for 32bit Java 6, which is required by Flex 4.1 SDK
export flexlib='/usr/local/flex_sdk_4.1'
${flexlib}/bin/mxmlc -creator "Hal Tily and Andrew Watts" -title "allapp" -localized-description "Artificial Language Learning Applet" en-US -compiler.debug -compiler.include-libraries=as3ds.swc -theme=${flexlib}/frameworks/themes/Halo/halo.swc allapp.mxml -output=allapp_flex_4.1.swf
